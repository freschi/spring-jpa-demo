insert into l_locations (l_name) values ('Vienna');
insert into l_locations (l_name) values ('Salzburg');

insert into d_devices (d_id, d_name, d_sensor_left, d_sensor_right, LOCATION_L_ID) values ('06501983746', 'Station VIE.A', 10, 10, 1);
insert into d_devices (d_id, d_name, d_sensor_left, d_sensor_right, LOCATION_L_ID) values ('06501983747', 'Station VIE.B', 12, 13, 1);
insert into d_devices (d_id, d_name, d_sensor_left, d_sensor_right, LOCATION_L_ID) values ('06501983748', 'Station VIE.C', 346, 234, 1);
insert into d_devices (d_id, d_name, d_sensor_left, d_sensor_right, LOCATION_L_ID) values ('06501983749', 'Station VIE.D', 546, 32, 1);
insert into d_devices (d_id, d_name, d_sensor_left, d_sensor_right, LOCATION_L_ID) values ('06501983750', 'Station VIE.E', 24, 284, 1);
insert into d_devices (d_id, d_name, d_sensor_left, d_sensor_right, LOCATION_L_ID) values ('06501983751', 'Station VIE.F', 545, 332, 1);
insert into d_devices (d_id, d_name, d_sensor_left, d_sensor_right, LOCATION_L_ID) values ('06501983752', 'Station SBG.A', 100, 834, 2);
insert into d_devices (d_id, d_name, d_sensor_left, d_sensor_right, LOCATION_L_ID) values ('06501983753', 'Station SBG.B', 100, 953, 2);
insert into d_devices (d_id, d_name, d_sensor_left, d_sensor_right, LOCATION_L_ID) values ('06501983754', 'Station SBG.C', 1123, 293, 2);
insert into d_devices (d_id, d_name, d_sensor_left, d_sensor_right, LOCATION_L_ID) values ('06501983755', 'Station SBG.D', 657, 230, 2);
insert into d_devices (d_id, d_name, d_sensor_left, d_sensor_right, LOCATION_L_ID) values ('06501983756', 'Station SBG.E', 438, 239, 2);
insert into d_devices (d_id, d_name, d_sensor_left, d_sensor_right, LOCATION_L_ID) values ('06501983757', 'Station SBG.F', 26, 186, 2);