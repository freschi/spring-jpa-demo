package at.resch.jpa_demo.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import at.resch.jpa_demo.domain.Device;

public interface DeviceService {
	
	public Page<Device> getDevicesWithSensors(int left, int right, Pageable pageable);
}
