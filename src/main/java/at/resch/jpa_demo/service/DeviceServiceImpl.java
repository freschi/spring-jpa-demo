package at.resch.jpa_demo.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Component;

import at.resch.jpa_demo.domain.Device;

@Component
public class DeviceServiceImpl implements DeviceService {

	private DeviceRepository deviceRepository;

	@Autowired
	public DeviceServiceImpl(DeviceRepository deviceRepository) {
		this.deviceRepository = deviceRepository;
	}

	@Override
	public Page<Device> getDevicesWithSensors(int left, int right,
			Pageable pageable) {
		return deviceRepository.findBySensorRightAndSensorLeft(left, right, pageable);
	}

	

}
