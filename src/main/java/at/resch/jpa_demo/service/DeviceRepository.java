package at.resch.jpa_demo.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.CrudRepository;

import at.resch.jpa_demo.domain.Device;

public interface DeviceRepository extends CrudRepository<Device, String> {
	
	Page<Device> findAll(Pageable pageable); 
	
	Page<Device> findBySensorRightAndSensorLeft(int leftSensor, int rightSensor, Pageable pageable);

}
