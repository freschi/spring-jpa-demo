package at.resch.jpa_demo.domain;

import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "l_locations")
public class Location {
	
	@Id
	@GeneratedValue
	@Column(name = "l_id")
	private int id;
	
	@Column(name = "l_name", nullable = false)
	private String name;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}


	@Override
	public String toString() {
		return "Location [id=" + id + ", name=" + name + "]";
	}
	
	

}
