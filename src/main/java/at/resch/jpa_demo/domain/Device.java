package at.resch.jpa_demo.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;


@Entity
@Table(name = "d_devices")
public class Device {
	
	@Id
	@Column(name = "d_id")
	private String id;
	
	@Column(name = "d_name")
	private String name;
	
	@Column(name = "d_sensor_left")
	private int sensorLeft;
	
	@Column(name = "d_sensor_right")
	private int sensorRight;
	
	@ManyToOne(optional = false)
	private Location location;
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getSensorLeft() {
		return sensorLeft;
	}
	public void setSensorLeft(int sensorLeft) {
		this.sensorLeft = sensorLeft;
	}
	public int getSensorRight() {
		return sensorRight;
	}
	public void setSensorRight(int sensorRight) {
		this.sensorRight = sensorRight;
	}
	@Override
	public String toString() {
		return "Device [id=" + id + ", name=" + name + ", sensorLeft="
				+ sensorLeft + ", sensorRight=" + sensorRight + ", location="
				+ location + "]";
	}
	public Location getLocation() {
		return location;
	}
	public void setLocation(Location location) {
		this.location = location;
	}
	
	
	

}
