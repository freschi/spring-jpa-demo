package at.resch.jpa_demo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

import at.resch.jpa_demo.domain.Device;
import at.resch.jpa_demo.service.DeviceRepository;
import at.resch.jpa_demo.service.DeviceService;

@Configuration
@ComponentScan
@EnableAutoConfiguration
public class JpaDemoApplication implements CommandLineRunner {

	@Autowired
	private DeviceRepository repository;
	
	@Autowired
	private DeviceService deviceService;
	
    public static void main(String[] args) {
        SpringApplication.run(JpaDemoApplication.class, args);
    }
    
    public void run(String ... args) {
    	System.out.println("Found " + repository.count() + " items");
    	for(Device d : repository.findAll()) {
    		System.out.println(d);
    	}
    	System.out.println();
    	System.out.println("Should be: Device [id=06501983746, name=Station VIE.A, sensorLeft=10, sensorRight=10, location=Location [id=1, name=Vienna]]");
    	System.out.println(deviceService.getDevicesWithSensors(10, 10, null).getContent().get(0));
    }
}
